<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'Test';
    protected $primaryKey = 'TestId';

    public function Test_Application(){
        return $this->hasMany('App\Test_Application','TestId','TestId');
    }

    public function Result(){
        return $this->hasMany('App\Result','TestId','TestId');
    }
}
