<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'Patient';
    protected $primaryKey = 'PatId';

    protected $fillable = ['Fname','Lname','Gender','Age','Weight','BloodPressure','Contact','Address'];

    public function User(){
        return $this->belongsTo('App\User','PatId','profileId');
    }

    public function Payment(){
        return $this->hasMany('App\Payment','PatId','PatId');
    }

    public function Test_Application(){
        return $this->hasMany('App\Test_Application','PatId','PatId');
    }
}
