<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],

        'accountantG' =>[
            'web',
            'accountant',
        ],

        'adminG' =>[
            'web',
            'admin',
        ],

        'patientG' =>[
            'web',
            'patient',
        ],

        'doctorG' =>[
            'web',
            'doctor',
        ],

        'labTechnicianG' =>[
            'web',
            'labTechnician',
        ],

        'labAttendantG' =>[
            'web',
            'labAttendant',
        ],
        'labdocG' =>[
            'web',
            'LabDoc',
        ]
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'doctor' => \App\Http\Middleware\Doctor::class,
        'accountant' => \App\Http\Middleware\Accountant::class,
        'admin' => \App\Http\Middleware\Admin::class,
        'labAttendant' => \App\Http\Middleware\LabAttendant::class,
        'labTechnician' => \App\Http\Middleware\LabTechnician::class,
        'patient' => \App\Http\Middleware\Patient::class,
        'LabDoc' => \App\Http\Middleware\LabDoc::class

    ];
}
