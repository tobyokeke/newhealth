<?php

namespace App\Http\Controllers;

use App\Dept;
use App\Http\Requests;
use App\Patient;
use App\Staff;
use App\Result;
use App\Payment;
use App\Test_Application;
use App\User;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'Patient'){
            $test = Test::all();
        }

        elseif(Auth::user()->role == 'Admin'){
            $test = Dept::all();
        }
        elseif(Auth::user()->role == 'Lab Technician'){
            $test = Test::all();
        }

        else {
            $test = '';
        }

        return view('home', ['test' => $test]);
    }

    public function viewPatients (){
        $patients = Patient::all();
        return view('viewPatients',['list' => $patients]);
    }

    public function viewPatientApplications(){
        $apps = Test_Application::where('PatId',Auth::user()->profileId)->get();

        return view('viewPatientsApps',['apps' => $apps]);
    }
    public function viewPatientDetails($id){
        $patient = Patient::where('PatId',$id)->get()->first();
        $user = User::where('profileId',$id)->get()->first();

        return view('viewPatientDetails', ['patient' => $patient, 'user' => $user]);
    }

    public function viewPatientTestResults($id){
        if($id != Auth::user()->profileId) return redirect('/');

        $patient = Patient::where('PatId',$id)->get()->first();
        $user = User::where('profileId',$id)->get()->first();
        return view('viewPatientDetails',['patient'=>$patient, 'user' => $user]);
    }
    public function viewTests(){
        $tests = Test_Application::all();

        return view('viewTests',['tests' => $tests]);
    }

    public function viewTestDetails($id){
        $test = Test_Application::find($id);
        $patid = $test->PatId;
        $testid = $test->TestId;
        $patient = Patient::where('PatId',$patid)->get()->first();
        $result = Result::where('TestId',$testid)->get()->first();

        return view('viewTestDetails',['test' => $test,'patient'=>$patient,'result'=>$result]);
    }

    public function viewPay($id){
        $apps = Test_Application::where('AppId',$id)->get()->first();

        return view('pay',['apps' => $apps]);
    }

    public function viewPayAccountant(){
        $payments = Payment::all();
        $payid = $this->makeAnArray($payments,'PatId');

        $patients = Patient::hydrateRaw("select p.Fname,p.Lname,pay.*,t.* from patient p inner join payment pay on p.PatId = pay.PatId
         inner join test_application ta on pay.AppId = ta.AppId inner join test t on ta.TestId = t.TestId where p.PatId in ".$payid);

        return view('viewPayAccountant',['payments' => $patients]);
    }

    public function viewUsersAdmin(){
        $users = User::all();

        return view('viewUsers',['users' => $users]);
    }

    public function viewPayAssignmentDetails($id){
        $payment = Payment::find($id);

        return view('viewPayAccountantDetails',['payment' => $payment]);
    }
    public function postNewPatient(Request $request){

        $patient = new Patient();
        $patient->Fname = $request->input('Fname');
        $patient->Lname = $request->input('Lname');
        $patient->Gender = $request->input('Gender');
        $patient->Age = $request->input('Age');
        $patient->Weight = $request->input('Weight');
        $patient->BloodPressure = $request->input('BloodPressure');
        $patient->Contact = $request->input('Contact');
        $patient->Address = $request->input('Address');
        $patient->Bmi = $request->input('Bmi');
        $patient->Manual1 = $request->input('Manual1');
        $patient->Manual2 = $request->input('Manual2');
        $patient->Waist = $request->input('Waist');
        $patient->Glucose = $request->input('Glucose');
        $patient->Cholesterol = $request->input('Cholesterol');
        $patient->Hcholesterol = $request->input('Hcholesterol');
        $patient->Risk = $request->input('Risk');
        $patient->Height = $request->input('Height');
        $patient->save();

        $patid = $patient->PatId;
        $user = new User();
        $user->name = $request->input('name');
        $user->password = Hash::make( $request->input('password'));
        $user->email = $request->input('email');
        $user->profileId = $patid;
        $user->save();


        return view('home', ['status' => 'Successfully Added Patient']);
    }


    public function postCreateTest(Request $request){

        $test = new Test();
        $test->TestName = $request->input('TestName');
        $test->Type = $request->input('Type');
        $test->Description = $request->input('Description');
        $test->Amount = $request->input('Amount');
        $test->save();

        return view('home', ['status' => 'Successfully Created Test']);

    }

    public function postApplyTest(Request $request){
        $testApp = new Test_Application();
        $testApp->PatId = $request->input('PatId');
        $testApp->TestId = $request->input('TestId');
        $testApp->save();

        return view('home',['status' => 'Successfully Applied for Test']);
    }

    public function postPay(Request $request){
        $id =  $request->input('id');
        $filename = $request->file('Document')->getClientOriginalName();

        $request->file('Document')->move('userUploads/payments/',$filename);


        $pay = new Payment();
        $pay->AppId = $id;
        $pay->PatId = Auth::user()->profileId;
        $pay->Document = 'http://localhost/newhealth/public/userUploads/payments/'. $filename;
        $pay->save();

        return redirect('/test/pay/' . $id );
    }

    public function postCreateDoctorReport(Request $request){
        $id = $request->input('id');
        $patid = $request->input('patient');
        $result = Result::find($id);
        $result->DoctorsReport = $request->input('DoctorsReport');
        $result->save();
        return redirect('/patient/'. $patid);
    }
    public function postCreateResult(Request $request){
        $result = new Result();
        $result->AppId = $request->input('appid');
        $result->TestId = $request->input('testid');
        $result->Report = $request->input('report');
        $result->Bmi = $request->input('Bmi');
        $result->Manual1 = $request->input('Manual1');
        $result->Manual2 = $request->input('Manual2');
        $result->Waist = $request->input('Waist');
        $result->Glucose = $request->input('Glucose');
        $result->Cholesterol = $request->input('Cholesterol');
        $result->Hcholesterol = $request->input('Hcholesterol');
        $result->Risk = $request->input('Risk');

        $result->StaffId = \Auth::user()->profileId;
        $result->save();

        mail($request->input('email'),"Your test result is ready!","Hi, ". $request->input('name').
            "\n Your test result from health lab is ready, Login to our site to see it.
             \n Thanks for your patronage.");


        return redirect('/test/details/' . $request->input('appid'));
    }
    public function postUpdatePatient (Request $request){
        $id = $request->input('id');
        $patient = Patient::where('PatId',$id)->get()->first();
        $patient->Fname = $request->input('Fname');
        $patient->Lname = $request->input('Lname');
        $patient->Gender = $request->input('Gender');
        $patient->Age = $request->input('Age');
        $patient->Weight = $request->input('Weight');
        $patient->BloodPressure = $request->input('BloodPressure');
        $patient->Contact = $request->input('Contact');
        $patient->Address = $request->input('Address');
        $patient->Height = $request->input('Height');
        $patient->save();

        $user= User::where('profileId',$id)->get()->first();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return redirect('/patient/'.$id);
    }

    public function postVerifyPayment($id){
        $payment = Payment::find($id);
        $payment->StaffId = Auth::user()->profileId;
        $payment->Status = 'Verified';
        $payment->save();

        return redirect('/view/payments');
    }

    public function postCreateStaff(Request $request){


        $staff = new Staff();
        $staff->Fname = $request->input('Fname');
        $staff->Lname = $request->input('Lname');
        $staff->Gender = $request->input('Gender');
        $staff->Age = $request->input('Age');
        $staff->Contact = $request->input('Contact');
        $staff->Specialization = $request->input('Specialization');
        $staff->DeptId = $request->input('DeptId');
        $staff->save();

        $staffid = $staff->StaffId;

        $user = new User();
        $user->name = $request->input('name');
        $user->password = Hash::make( $request->input('password'));
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->profileId = $staffid;
        $user->save();

        $dept = Dept::all();

        return view('home',['status' => 'Successfully Created Staff', 'test' => $dept]);

    }

    public function postCreateDept(Request $request){

        $dept = new Dept();
        $dept->DepartmentName = $request->input('DepartmentName');
        $dept->save();
        $dept = Dept::all();

        return view('home',['status' => 'Successfully Created Department', 'test' => $dept]);
    }

    public function postDeleteUser($id){
        $user = User::find($id);

        $staff = Staff::where('StaffId',$user->profileId)->delete();

        $user->delete();

        return redirect('/admin/view/users');

    }

    public function makeAnArray($data,$id){
        $appIds = "";

        foreach ($data as $item){
            $appIds .= $item->$id .',';
        }
        $appIds .= "0";
        $appIdsInBracket = "(".$appIds.")";
        return $appIdsInBracket;

    }

    public function removeSlashes($array,$property){
        foreach($array as $item) {
            echo stripslashes($item->$property);
        }
        return $array;
    }



}
