<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/register', function(){ return redirect('/'); });
    Route::get('/', 'HomeController@index');
    
    Route::get('/home', 'HomeController@index');
});

Route::group(['middleware' =>'doctorG'], function(){

//    Route::get('/patients', 'HomeController@viewPatients');


    Route::post('/doctor/report/create', 'HomeController@postCreateDoctorReport');

});

Route::group(['middleware' =>'adminG'], function(){
    Route::get('/admin/view/users', 'HomeController@viewUsersAdmin');

    Route::post('/user/delete/{id}', 'HomeController@postDeleteUser');
    Route::post('/staff/create', 'HomeController@postCreateStaff');
    Route::post('/dept/create', 'HomeController@postCreateDept');

});

Route::group(['middleware' =>'accountantG'], function(){
    Route::get('/view/payments', 'HomeController@viewPayAccountant');

    Route::post('/payment/verify/{id}', 'HomeController@postVerifyPayment');
});

Route::group(['middleware' =>'patientG'], function(){

    Route::get('/view/applications', 'HomeController@viewPatientApplications');
    Route::get('/test/pay/{id}', 'HomeController@viewPay');
    Route::get('/payments/verify/{id}', 'HomeController@viewPayAccountantDetails');
    Route::get('/patient/result/{id}','HomeController@viewPatientTestResults');



    Route::post('/test/pay/', 'HomeController@postPay');
    Route::post('/test/apply', 'HomeController@postApplyTest');

});

Route::group(['middleware' =>'labTechnicianG'], function(){

    Route::get('/tests/view', 'HomeController@viewTests');
    Route::get('/test/details/{id}', 'HomeController@viewTestDetails');

    Route::post('/test/result/create', 'HomeController@postCreateResult');
    Route::post('/test/create', 'HomeController@postCreateTest');

});

Route::group(['middleware' =>'labAttendantG'], function(){

    Route::post('/home', 'HomeController@postNewPatient');
    Route::post('/patient/update', 'HomeController@postUpdatePatient');

});

Route::group(['middleware' =>'labdocG'], function(){
    Route::get('/patients', 'HomeController@viewPatients');
    Route::get('/patient/{id}', 'HomeController@viewPatientDetails');
});


