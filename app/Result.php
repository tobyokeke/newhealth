<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'Result';
    protected $primaryKey = 'ResId';

    public function Test_Application(){
        return $this->belongsTo('App\Test_Application','AppId','AppId');
    }
}
