<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'Staff';
    protected $primaryKey = 'StaffId';

    public function User(){
        return $this->belongsTo('App\User','StaffId','profileId');
    }

    public function Test_Application(){
        return $this->hasOne('App\Test_Application','StaffId','StaffId');
    }

    public function Dept(){
        return $this->belongsTo('App\Dept','DeptId','DeptId');
    }

}
