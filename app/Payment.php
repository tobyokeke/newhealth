<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'Payment';
    protected $primaryKey = 'PayId';

    public function Patient(){
        return $this->belongsToMany('App\Patient','PatId','PatId');
    }

    public function Test_Application(){
        return $this->belongsTo('App/Test_Application','AppId','AppId');
    }



}
