<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test_Application extends Model
{
    protected $table = 'Test_Application';
    protected $primaryKey = 'AppId';

    public function Test(){
        return $this->belongsTo('App\Test','TestId','TestId');
    }

    public function Patient(){
        return $this->belongsTo('App\Patient','PatId','PatId');
    }

    public function Staff(){
        return $this->belongsTo('App\Staff','StaffId','StaffId');
    }
    public function Result(){
        return $this->hasOne('App\Result','AppId','AppId');
    }

    public function Payment(){
        return $this->hasOne('App\Payment','AppId','AppId');
    }
}
