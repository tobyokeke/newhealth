<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('role',['Patient','Lab Attendant','Lab Technician','Accountant','Doctor','Admin']);
            $table->integer('profileId');
            $table->rememberToken();
            $table->timestamps();
        });


        Schema::create('Password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });

        Schema::create('Dept', function (Blueprint $table){
            $table->increments('DeptId');
            $table->string('DepartmentName');
            $table->timestamps();

        });

        Schema::create('Patient',function(Blueprint $table){
            $table->increments('PatId');
            $table->string('Fname',25);
            $table->string('Lname',25);
            $table->string('Gender',6);
            $table->integer('Age');
            $table->integer('Weight');
            $table->integer('BloodPressure');
            $table->string('Contact');
            $table->string('Address');
            $table->string('Height');

            $table->timestamps();

        });

        Schema::create('Test', function (Blueprint $table){
            $table->increments('TestId');
            $table->string('TestName');
            $table->string('Type');
            $table->string('Description');
            $table->integer('Amount');
            $table->timestamps();
        });


        Schema::create('Staff', function (Blueprint $table){
            $table->increments('StaffId');
            $table->string('Fname');
            $table->string('Lname');
            $table->string('Gender');
            $table->string('Age');
            $table->string('Contact');
            $table->string('Specialization');
            $table->timestamps();
            $table->integer('DeptId',false,true);

            $table->foreign('DeptId')->references('DeptId')->on('Dept')->onUpdate('cascade')
                ->onDelete('no action');

        });



        Schema::create('Result', function (Blueprint $table){
            $table->increments('ResId');
            $table->integer('AppId',false,true);
            $table->integer('TestId',false,true);
            $table->string('Report',5000);
            $table->string('DoctorsReport',5000);
            $table->string('Bmi');
            $table->string('Manual1');
            $table->string('Manual2');
            $table->string('Waist');
            $table->string('Glucose');
            $table->string('Cholesterol');
            $table->string('Hcholesterol');
            $table->string('Risk');

            $table->timestamps();
            $table->integer('StaffId',false,true);

            $table->foreign('AppId')->references('AppId')->on('Test_Application')->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('StaffId')->references('StaffId')->on('Staff')->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('TestId')->references('TestId')->on('Test')->onUpdate('cascade')
                ->onDelete('no action');
        });


        Schema::create('Test_Application', function (Blueprint $table){
            $table->increments('AppId');
            $table->integer('PatId',false,true);

            $table->integer('TestId',false,true);
            $table->integer('Total');

            $table->timestamps();
            $table->integer('StaffId',false,true);

            $table->foreign('PatId')->references('PatId')->on('Patient')->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('StaffId')->references('StaffId')->on('Staff')->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('TestId')->references('TestId')->on('Test')->onUpdate('cascade')
                ->onDelete('no action');


        });

        Schema::create('Payment', function (Blueprint $table){
            $table->increments('PayId');
            $table->integer('PatId',false,true);
            $table->integer('AppId',false,true);
            $table->string('Document');
            $table->enum('Status',['Not Verified','Verified']);
            $table->timestamps();

            $table->integer('StaffId',false,true)->nullable();

            $table->foreign('PatId')->references('PatId')->on('Patient')->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('AppId')->references('AppId')->on('Test_Application')->onUpdate('cascade')
                ->onDelete('no action');

            $table->foreign('StaffId')->references('StaffId')->on('Staff')->onUpdate('cascade')
                ->onDelete('no action');


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
