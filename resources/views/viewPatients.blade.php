@extends('layouts.app')

@section('content')

    <div class="container">

        <table class="table table-responsive table-hover">
            <tr>
                <th>First Name</th>
                <th>Surname</th>
                <th>Gender</th>
                <th>Created</th>
                <th></th>
            </tr>

        @if(isset($list))
            @foreach($list as $item)
            <tr>
                <td>{{$item->Fname}}</td>
                <td>{{$item->Lname}}</td>
                <td>{{$item->Gender}}</td>
                <td>{{$item->created_at}}</td>

                <td><a href="{{url('/patient/'. $item->PatId)}}">
                        <button class="btn btn-primary">View</button>
                    </a>
                </td>
            </tr>
            @endforeach
        @endif
        </table>
    </div>

@endsection