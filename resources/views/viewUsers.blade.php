@extends('layouts.app')

@section('content')

     <div class="container">
         @if(isset($users))

             <table class="table table-responsive table-hover">
                 <tr>
                     <th>Username</th>
                     <th>Role</th>
                     <th>Email</th>
                     <th>Date Created</th>
                     <th></th>
                 </tr>
                @foreach($users as $item)
                    @if($item->role != 'Patient')
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->role}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            <form method="post" action="{{url('/user/delete/'. $item->id)}}">
                               <input type="hidden" name="_token" value="{{csrf_token()}}">

                                <button class="btn btn-primary" type="submit">Delete</button>
                            </form>

                        </td>
                    </tr>
                     @endif
                @endforeach

             </table>

         @endif
     </div>

@endsection