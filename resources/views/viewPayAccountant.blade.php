@extends('layouts.app')

@section('content')

     <div class="container">
         @if(isset($payments))
             <table class="table table-responsive table-hover">
                 <tr>
                     <th>Patient Name</th>
                     <th>Test Applied for</th>
                     <th>Attached Doc</th>
                     <th>Amount</th>
                     <th></th>
                 </tr>

                 @foreach($payments as $item)
                     <tr>
                         <td>{{$item->Fname}} {{$item->Lname}}</td>
                         <td>{{$item->TestName}}</td>
                         <th>
                             <a href="{{$item->Document}}"> Download </a>
                         </th>
                         <td>{{$item->Amount}}</td>
                         <td>
                             @if($item->Status == 'Not Verified')
                             <form method="post" action="{{url('/payment/verify/' . $item->PayId)}}">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                 <button class="btn btn-primary" type="submit">Verify</button>

                             </form>
                             @else
                                 Verified
                             @endif
                         </td>
                     </tr>
                 @endforeach
             </table>
         @endif
     </div>

@endsection