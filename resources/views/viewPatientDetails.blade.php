@extends('layouts.app')

@section('content')

    <div class="container">

        @if(Auth::user()->role == 'Lab Attendant')
        <h2 align="center">Update Patient Record</h2>
        @endif

        @if(Auth::user()->role == 'Doctor')
            <img src="{{url('/img/logo.png')}}" style="height: 60px; width:60px; float:left;">
                PRIMECARE <br>
                <span style="color:dodgerblue"> MEDICAL </span>
                <span style="color:red"> CENTER</span>
            <h2 align="center">View Patient Record</h2>
        @endif

        @if(isset($patient) && isset($user))
        <div class="row">
            <div class="col-md-4" >
                <table class="table table-bordered" >
                    <tr >
                        <th>FULL NAME <br> (PLEASE PRINT)</th>
                        <td>{{$patient->Fname}} {{$patient->Lname}}</td>
                    </tr>

                    <tr>
                        <th>SEX</th>
                        <th>DATE OF BIRTH</th>
                    </tr>
                    <tr>
                        <td>{{$patient->Gender}}</td>
                        <td>11/2/12</td>
                    </tr>
                </table>

                <hr style="border-color: red;">
                <p style="font-size: 13px;text-align: center;color:red;">DO NOT WRITE BELOW THIS LINE</p>
                <p align="center"><b>BIOMETRICS</b></p>
                <hr style="border-color: red;margin-top: 0">
                <p style="color:red;">
                    <span style="width:150px;display: inline-block"> BLOOD PRESSURE :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->BloodPressure}}</span> <br>

                    <span style="width:150px;display: inline-block">AGE :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Age}}</span> <br>

                    {{--<span style="width:150px;display: inline-block">HEIGHT :</span>--}}
                    {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Height}}</span> <br>--}}

                    <span style="width:150px;display: inline-block">WEIGHT :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Weight}}</span> <br>

                    <span style="width:150px;display: inline-block">CONTACT :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Contact}}</span> <br>

                    <span style="width:150px;display: inline-block">ADDRESS :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Address}}</span> <br>

                    <span style="width:150px;display: inline-block">DATE ADDED :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->created_at}}</span> <br>
                    <span style="width:150px;display: inline-block">DATE LAST UPDATED :</span>
                    <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->updated_at}}</span> <br>
                </p>
                <hr style="border-color: blue;margin-top: 0">
                @if(Auth::user()->role == 'Lab Attendant')

                    {{--@foreach($patient->Test_Application->where('PatId',$patient->PatId) as $item)--}}

                    {{--<p><b>FURTHER TESTS</b></p>--}}
                    {{--<hr style="border-color: red;margin-top: 0">--}}
                    {{--<p style="color:red;">--}}
                        {{--<span style="width:150px;display: inline-block"> BMI :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Bmi}}</span> <br>--}}

                        {{--<span style="width:150px;display: inline-block">Manual 1 :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Manual1}}</span> <br>--}}

                        {{--<span style="width:150px;display: inline-block">Manual 2 :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Manual2}}</span> <br>--}}

                        {{--<span style="width:150px;display: inline-block">Waist :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Waist}}</span> <br>--}}

                        {{--<span style="width:150px;display: inline-block">Glucose :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Glucose}}</span> <br>--}}

                        {{--<span style="width:150px;display: inline-block">Hcholesterol :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Hcholesterol}}</span> <br>--}}

                        {{--<span style="width:150px;display: inline-block">Risk :</span>--}}
                        {{--<span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Risk}}</span> <br>--}}

                    {{--</p>--}}
                {{--@endforeach--}}

                @endif

            </div>

            <div class="col-md-8">

            @if(Auth::user()->role == 'Lab Attendant')

                <style>
                    label {color:black;}
                </style>
                <form class="form-group" method="post" action="{{url('/patient/update')}}">
                <input type="hidden" value="{{csrf_token()}}" name="_token">
                <input type="hidden" value="{{$patient->PatId}}" name="id">

                <label >Username:</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}">

                <label >Email:</label>
                <input type="text" class="form-control" name="email" value="{{$user->email}}">

                <label >First Name:</label>
                <input type="text" class="form-control" name="Fname" value="{{$patient->Fname}}">

                <label >Last Name:</label>
                <input type="text" class="form-control" name="Lname" value="{{$patient->Lname}}">

                <label >Gender:</label>
                <input type="text" class="form-control" name="Gender" value="{{$patient->Gender}}">

                <label >Age:</label>
                <input type="text" class="form-control" name="Age" value="{{$patient->Age}}">

                <label >Weight:</label>
                <input type="text" class="form-control" name="Weight" value="{{$patient->Weight}}">

                <label >Blood Pressure:</label>
                <input type="text" class="form-control" name="BloodPressure" value="{{$patient->BloodPressure}}">

                <label >Height:</label>
                <input type="text" class="form-control" name="Height" value="{{$patient->Height}}">

                <label >Contact:</label>
                <input type="text" class="form-control" name="Contact" value="{{$patient->Contact}}">

                <label >Address:</label>
                <textarea class="form-control" name="Address">{{$patient->Address}}</textarea> <br>




                <button class="btn btn-primary" type="submit">Update</button>
            </form>
            @endif

            @if( Auth::user()->role == 'Doctor' || Auth::user()->role == "Patient")

                @foreach($patient->Test_Application->where('PatId',$patient->PatId) as $item)


                    <table class="table table-bordered" >
                        <tr >
                            <th><b>Test Taken</b> <br> (PLEASE PRINT)</th>
                            <td>{{ $item->Test->TestName}}</td>
                        </tr>

                        <tr>
                             <th>Lab Technicians Comment</th>
                             <td>{{ $item->Result->Report}}</td>
                        </tr>
                        <tr>
                            <th>Date Applied</th>
                            <td>{{$item->Result->created_at}}</td>
                        </tr>
                        @if(isset($item->Result->DoctorsReport))
                        <tr>
                            <th>Doctors Comment</th>
                            <td>{{$item->Result->DoctorsReport}}</td>
                        </tr>
                        @endif
                    </table>


                    <hr style="border-color: red;margin-top: 0">
                    <p style="color:red;">
                        <span style="width:150px;display: inline-block"> BMI :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Bmi}}</span> <br>

                        <span style="width:150px;display: inline-block">Manual 1 :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Manual1}}</span> <br>

                        <span style="width:150px;display: inline-block">Manual 2 :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Manual2}}</span> <br>

                        <span style="width:150px;display: inline-block">Waist :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Waist}}</span> <br>

                        <span style="width:150px;display: inline-block">Glucose :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Glucose}}</span> <br>

                        <span style="width:150px;display: inline-block">Hcholesterol :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Hcholesterol}}</span> <br>

                        <span style="width:150px;display: inline-block">Risk :</span>
                        <span style="margin-left:10px;text-decoration: underline;color:black">{{$item->Result->Risk}}</span> <br>

                    </p>

                        @if(!isset($item->Result->DoctorsReport) && Auth::user()->role == 'Doctor')
                            <form method="post" action="{{url('/doctor/report/create')}}">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="patient" value="{{$patient->PatId}}">
                                <input type="hidden" name="id" value="{{$item->Result->ResId}}">

                                <label style="color:black">Your Comment:</label>
                                <input type="text" class="form-control" name="DoctorsReport">
                                <br>
                                <button class="btn btn-primary" type="submit">Send</button>
                            </form> <br>

                        @endif

                        <br>



                    @endforeach

                @endif

            </div>
        @endif

        </div>
    </div>
@endsection