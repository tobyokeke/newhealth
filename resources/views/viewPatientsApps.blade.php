@extends('layouts.app')

@section('content')

     <div class="container col-md-12">
         <table class="table table-responsive table-hover">
             <tr>
                 <th>Test Name</th>
                 <th>Amount</th>
                 <th>Date Applied</th>
                 <th>Doctors Comment</th>
                 <th>Result Released</th>
                 <th></th>
             </tr>

             @foreach($apps as $item)
                <tr>
                    <td>{{$item->Test->TestName}}</td>
                    <td>{{$item->Test->Amount}}</td>
                    <td>{{$item->Test->created_at}}</td>

                    @if(!isset($item->Payment->Document))
                        <td>Please make payment</td>
                        <td></td>
                    @elseif($item->Payment->Status == 'Not Verified')
                        <td>Payment Verification Pending</td>
                        <td></td>
                    @else
                        @if($item->Result)
                            @if($item->Result->DoctorsReport)
                                <td>{{$item->Result->DoctorsReport}}</td>
                                <td>{{$item->Result->created_at}}</td>
                            @else
                                <td>Result Ready but the doctor hasn't commented yet</td>
                                <td>{{$item->Result->created_at}}</td>
                            @endif
                        @else
                            <td>Result not ready</td>
                            <td></td>
                        @endif
                    @endif

                    <td>
                        @if(!isset($item->Payment->Document))
                        <a href="{{url('test/pay/' . $item->AppId)}}">
                            <button class="btn btn-primary">Pay</button>
                        </a>
                            @else
                            @if($item->Result)
                                <a href="{{url('/patient/result/'. Auth::user()->profileId)}}">
                                    <button class="btn btn-primary">View Result</button>
                                </a>
                            @endif
                        @endif
                    </td>
                </tr>

             @endforeach
         </table>
     </div>

@endsection