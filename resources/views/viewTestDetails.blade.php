@extends('layouts.app')

@section('content')

     <div class="container">

             @if(isset($test))
             <img src="{{url('/img/logo.png')}}" style="height: 60px; width:60px; float:left;">
             PRIMECARE <br>
             <span style="color:dodgerblue"> MEDICAL </span>
             <span style="color:red"> CENTER</span>

            <h3 align="center">Patient Test Result</h3>

             <table class="table table-bordered" >
                 <tr >
                     <th>FULL NAME <br> (PLEASE PRINT)</th>
                     <td>{{$patient->Fname}} {{$patient->Lname}}</td>
                 </tr>

                 <tr>
                     <th>SEX</th>
                     <th>DATE OF BIRTH</th>
                 </tr>
                 <tr>
                     <td>{{$patient->Gender}}</td>
                     <td>11/2/12</td>
                 </tr>
                 <tr>
                     <th>TEST NAME</th>
                     <th>TEST TYPE</th>
                 </tr>
                 <tr>
                     <td>{{$test->Test->TestName}}</td>
                     <td>{{$test->Test->Type}}</td>
                 </tr>
                 <tr>
                     <th>Date Applied</th>
                     <th>RESULT STATUS</th>
                 </tr>
                 <tr>
                     <td>{{$test->created_at}}</td>
                     <td>@if(isset($test->Result->Report))
                             Result is Ready @else Result is not Ready
                         @endif
                     </td>
                 </tr>
             </table>

             <hr style="border-color: red;">
             <p style="font-size: 13px;text-align: center;color:red;">DO NOT WRITE BELOW THIS LINE</p>
             <p align="center"><b>BIOMETRICS</b></p>
             <hr style="border-color: red;margin-top: 0">
         <div class="row">

             <div class="col-md-6">

             <p style="color:red;">
                 <span style="width:150px;display: inline-block"> BLOOD PRESSURE :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->BloodPressure}}</span> <br>

                 <span style="width:150px;display: inline-block">AGE :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Age}}</span> <br>

                 <span style="width:150px;display: inline-block">HEIGHT :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Height}}</span> <br>

                 <span style="width:150px;display: inline-block">WEIGHT :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Weight}}</span> <br>

                 <span style="width:150px;display: inline-block">CONTACT :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Contact}}</span> <br>

                 <span style="width:150px;display: inline-block">ADDRESS :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->Address}}</span> <br>

                 <span style="width:150px;display: inline-block">DATE ADDED :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->created_at}}</span> <br>
                 <span style="width:150px;display: inline-block">DATE LAST UPDATED :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$patient->updated_at}}</span> <br>
             </p>
             </div> <!-- end left section-->


             @if(isset($test->Result->Report))
             <div class="col-md-6">

             <p style="color:red;">
                 <span style="width:150px;display: inline-block"> BMI :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Bmi}}</span> <br>

                 <span style="width:150px;display: inline-block">Manual 1 :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Manual1}}</span> <br>

                 <span style="width:150px;display: inline-block">Manual 2 :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Manual2}}</span> <br>

                 <span style="width:150px;display: inline-block">Waist :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Waist}}</span> <br>

                 <span style="width:150px;display: inline-block">Glucose :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Glucose}}</span> <br>

                 <span style="width:150px;display: inline-block">Hcholesterol :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Hcholesterol}}</span> <br>

                 <span style="width:150px;display: inline-block">Risk :</span>
                 <span style="margin-left:10px;text-decoration: underline;color:black">{{$result->Risk}}</span> <br>
             </p>
             </div>

         </div>
             @endif

                @if(!isset($test->Result->Report))
                    <style> label {color:black;} </style>
                <form method="post" action="{{url('/test/result/create')}}" >
                   <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="appid" value="{{$test->AppId}}">
                    <input type="hidden" name="testid" value="{{$test->TestId}}">
                    <input type="hidden" name="email" value="{{$test->Patient->User->email}}">
                    <input type="hidden" name="name" value="{{$test->Patient->User->Fname .' '
                    . $test->Patient->User->Sname}}">

                    <label>Manual 1:</label>
                    <input type="text" class="form-control" name="Manual1" value="{{$patient->Manual1}}">

                    <label >Manual 2:</label>
                    <input type="text" class="form-control" name="Manual2" value="{{$patient->Manual2}}">

                    <label >Bmi:</label>
                    <input type="text" class="form-control" name="Bmi" value="{{$patient->Bmi}}">

                    <label >Waist:</label>
                    <input type="text" class="form-control" name="Waist" value="{{$patient->Waist}}">

                    <label >Glucose:</label>
                    <input type="text" class="form-control" name="Glucose" value="{{$patient->Glucose}}">

                    <label >Cholesterol:</label>
                    <input type="text" class="form-control" name="Cholesterol" value="{{$patient->Cholesterol}}">

                    <label >Hcholesterol:</label>
                    <input type="text" class="form-control" name="Hcholesterol" value="{{$patient->Hcholesterol}}">

                    <label >Risk:</label>
                    <input type="text" class="form-control" name="Risk" value="{{$patient->Risk}}">



                    <label class="label">Report</label>
                    <textarea class="form-control" name="report" rows="15" placeholder="Type test comment here"></textarea> <br>
                    <button type="submit" class="btn btn-success">Add Result</button>
                </form>
                @endif
         @endif


     </div>

@endsection