@extends('layouts.app')

@section('content')

     <div class="container">
         <table class="table table-responsive table-hover">
             <tr>
                 <th>Test Name</th>
                 <th>Applicant Name</th>
                 <th>Date Applied</th>
                 <th></th>
             </tr>

             @if(isset($tests))
                 @foreach($tests as $item)
                     <tr>
                         <td>{{$item->Test->TestName}}</td>
                         <td>{{$item->Patient->Fname . " " . $item->Patient->Lname}}</td>
                         <td>{{$item->created_at}}</td>
                         <td><a href="{{url('/test/details/' . $item->AppId)}}">
                                 <button class="btn btn-primary" type="submit">
                                     View
                                 </button>
                             </a></td>
                     </tr>

                 @endforeach
             @endif
         </table>
     </div>

@endsection