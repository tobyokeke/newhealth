@extends('layouts.app')

@section('content')

     <div class="container">
        @if(isset($apps))
            Test Name: {{$apps->Test->TestName}} <br>
            Type: {{$apps->Test->Type}} <br>
            Amount: {{$apps->Test->Amount}} <br>

         @if(isset($apps->Payment->Document))
             Payment Status: Reciept submitted for Approval <br>
             Attached document: <a href="{{$apps->Payment->Document}}">Click to download</a>
         @endif

         @if(!isset($apps->Payment))

         <form method="post" action="{{url('/test/pay')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="id" value="{{$apps->AppId}}">

            <label>Upload Receipt</label>
            <input type="file" class="form-control" name="Document"> <br>

            <button class="btn btn-primary">Upload</button>
         </form>
         @endif


         @endif
     </div>

@endsection