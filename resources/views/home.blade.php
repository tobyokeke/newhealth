@extends('layouts.app')

@section('content')



    <div class="container" >
        <div class="row">
            <div class="col-md-8 col-md-offset-1" >
                <div class="panel panel-default" style="padding: 15px;">


                    @if(isset($status))
                        <p class="alert alert-success"> {{$status}}</p>
                    @endif

                    <br>

                    @if(Auth::user()->role == 'Doctor')

                        <h3 align="center">Welcome Doctor, {{Auth::user()->Staff->Fname .' '. Auth::user()->Staff->Lname}}!</h3>

                    @endif


                    @if(Auth::user()->role == 'Accountant')

                        <h3 align="center">Welcome Accountant, {{Auth::user()->Staff->Fname .' '. Auth::user()->Staff->Lname}}!</h3>

                    @endif


                    @if(Auth::user()->role == 'Lab Attendant')
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Add Patient
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">

                                        @if(Auth::user()->role == "Patient")
                                            <h1>Welcome {{Auth::user()->name}}</h1>
                                        @endif
                                        @if(Auth::user()->role == "Lab Attendant")

                                            <form class="form-group" method="post" action="{{url('/home')}}">
                                                <input type="hidden" value="{{csrf_token()}}" name="_token">

                                                <label >Username:</label>
                                                <input type="text" class="form-control" name="name">

                                                <label >Email:</label>
                                                <input type="text" class="form-control" name="email">

                                                <label >Password:</label>
                                                <input type="text"  class="form-control" name="password">

                                                <label >First Name:</label>
                                                <input type="text" class="form-control" name="Fname">

                                                <label >Last Name:</label>
                                                <input type="text" class="form-control" name="Lname">

                                                <label >Gender:</label>
                                                <input type="text" class="form-control" name="Gender">

                                                <label >Age:</label>
                                                <input type="text" class="form-control" name="Age">

                                                <label >Weight:</label>
                                                <input type="text" class="form-control" name="Weight">

                                                <label >Blood Pressure:</label>
                                                <input type="text" class="form-control" name="BloodPressure">

                                                <label >Contact:</label>
                                                <input type="text" class="form-control" name="Contact">

                                                <label >Address:</label>
                                                <textarea class="form-control" name="Address"></textarea> <br>




                                                <button class="btn btn-primary" type="submit">Add Patient</button>
                                            </form>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            @endif

                            @if(Auth::user()->role == 'Admin')
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Create Department
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <form method="post" action="{{url('/dept/create')}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                <label >Department Name:</label>
                                                <input type="text" class="form-control" name="DepartmentName">
                                                <br>

                                                <button class="btn btn-primary" type="submit">Add</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                Create Staff
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body">
                                            <form method="post" action="{{url('/staff/create')}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                <label >Username:</label>
                                                <input type="text" class="form-control" name="name">

                                                <label >Email:</label>
                                                <input type="text" class="form-control" name="email">

                                                <label >Role:</label>
                                                <select name="role" class="form-control">
                                                    <option value="Admin">Admin</option>
                                                    <option value="Doctor">Doctor</option>
                                                    <option value="Accountant">Accountant</option>
                                                    <option value="Lab Technician">Lab Technician</option>
                                                    <option value="Lab Attendant">Lab Attendant</option>
                                                    <option value="Patient">Patient</option>

                                                </select>

                                                <label >Password:</label>
                                                <input type="text"  class="form-control" name="password">

                                                <label >First Name:</label>
                                                <input type="text" class="form-control" name="Fname">

                                                <label >Last Name:</label>
                                                <input type="text" class="form-control" name="Lname">

                                                <label >Gender:</label>
                                                <input type="text" class="form-control" name="Gender">

                                                <label >Age:</label>
                                                <input type="text" class="form-control" name="Age">

                                                <label >Contact:</label>
                                                <input type="text" class="form-control" name="Contact">

                                                <label>Specialization</label>
                                                <input type="text" class="form-control" name="Specialization">

                                                <label>Department</label>
                                                <select name="DeptId" class="form-control">
                                                    @foreach($test as $item)
                                                        <option value="{{$item->DeptId}}">{{$item->DepartmentName}}</option>
                                                    @endforeach
                                                </select>
                                                <br>
                                                <button type="submit" class="btn btn-primary">Create</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>


                            @endif

                            @if(Auth::user()->role == 'Lab Technician')

                                <h4> Tests available :</h4>
                                <select class="form-control" >
                                    @foreach($test as $item)
                                        <option>{{$item->TestName}}</option>
                                    @endforeach
                                </select> <br><br>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Create Test
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <form method="post" action="{{url('/test/create')}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                <label>Name</label>
                                                <input type="text" class="form-control" name="TestName">

                                                <label>Type</label>
                                                <input type="text" class="form-control" name="Type">

                                                <label>Description</label>
                                                <input type="text" class="form-control" name="Description">

                                                <label>Amount</label>
                                                <input type="text" class="form-control" name="Amount"> <br>

                                                <button type="submit" class="btn btn-primary">Add</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            @endif

                            @if(Auth::user()->role == 'Patient' && isset($test))
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Apply for Test
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <form method="post" action="{{url('/test/apply')}}">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="hidden" name="PatId" value="{{Auth::user()->profileId}}">
                                                <select name="TestId" class="form-control">
                                                    @foreach($test as $item)
                                                        <option value="{{$item->TestId}}">{{$item->TestName}} : {{$item->Amount}}</option>
                                                    @endforeach
                                                </select>
                                                <br>

                                                <button type="submit" class="btn btn-primary">Apply</button>
                                            </form>


                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>


                </div>
            </div>

        </div>
        <div class="col-md-3 hidden-sm hidden-xs" style="position:absolute; margin: 0px 10px;right:10px;top: 120px; min-height: 500px; background-color: #1b6d85;">
            <br><br><br><br>
            <h3 style="color: white;">Company News!</h3>
            <br>
            <p style="background-color:lightblue; height: 200px; font-size: 16px; border-radius: 10px; padding: 10px;">
                This is company wide news.
            </p>

        </div>

        <div class="col-sm-12 hidden-md hidden-lg" style=" background-color: #1b6d85; padding: 10px; border-radius: 2px">
            <h3 style="color: white;">Company News!</h3>
            <p style="background-color:lightblue; height: 200px; font-size: 16px; border-radius: 10px; padding: 10px;">
                This is company wide news.
            </p>

        </div>

    </div>
@endsection