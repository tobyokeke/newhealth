<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Health</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
     <link href="{{ url('css/app.css') }}" rel="stylesheet">
    <link href="{{ url('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <style>
        body {
            font-family: 'Lato';
            color:black;
        }

        .fa-btn {
            margin-right: 6px;
        }

         .panel{
             background-color: lightblue ;
         }
        .panel-title{

            color: white;
        }
        .panel-default > .panel-heading{
            background-color: black;
            color:white;
        }
        label{
            color:white;
        }
        div.row{
            margin-top:50px;
        }

        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
            background-color: #000;
            color:white;
        }
        nav
        {
            font-size: 15px;
        }
    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body id="wrapper">


<nav class="navbar navbar-default" style="border-bottom: 7px solid deepskyblue;">
    <div class="container">
        <div class="navbar-header" style="height:100px;">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{url('/img/logo.png')}}" style="height: 60px; width:60px; float:left;">
                    PRIMECARE <br>
                    <span style="color:dodgerblue"> MEDICAL </span>
                    <span style="color:red"> CENTER</span>


            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}">Home</a></li>

                @if(!Auth::guest())

                    @if(Auth::user()->role == 'Admin')
                        <li><a href="{{ url('/admin/view/users') }}">View Users</a></li>
                    @endif

                    @if(Auth::user()->role == 'Accountant')
                        <li><a href="{{ url('/view/payments') }}">View Patients</a></li>
                    @endif

                    @if(Auth::user()->role == 'Lab Attendant')
                            <li><a href="{{ url('/patients') }}">View Patients</a></li>
                        @endif

                    @if(Auth::user()->role == 'Patient')
                        <li><a href="{{ url('/view/applications') }}">View Previous Applications</a></li>
                    @endif


                    @if(Auth::user()->role == 'Doctor')
                        <li><a href="{{ url('/patients') }}">View Patients Results</a></li>
                    @endif

                    @if(Auth::user()->role == 'Lab Technician')
                        <li><a href="{{ url('/tests/view') }}">View Test Applications</a></li>
                    @endif
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>

                @else

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            @if(Auth::user()->role == "Doctor" || Auth::user()->role == "Lab Technician")
                                <button class="btn btn-primary" onclick="window.print();return false;">Print</button>
                            @endif
                            &nbsp; &nbsp;
                            <i class="fa fa-user" style="font-size: 24px; color: deepskyblue;"></i>
                            {{ Auth::user()->name }} ( {{Auth::user()->role}} ) <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>

                @endif
            </ul>
        </div>
    </div>
</nav>
        <div style="min-height: 500px">
        @yield('content')
        </div>

        <p class="panel" align="center" style="margin-top: 10px">&copy 2016. PRIME CARE <span style="color:darkred;">MEDICAL</span> CENTER .</p>
        <!-- JavaScripts -->
<script src="{{url('js/jquery.js')}}"></script>
<script src="{{url('js/bootstrap.min.js')}}"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>